// alert('hi')


const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);

const fullAddress = ['258 Washington Ave NW', 'California', '90011']

const [street, state, zipCode] = fullAddress;
console.log(`I live at ${street}, ${state} ${zipCode}`)

const animal = {
	name: 'Lolong',
	breed: 'saltwater crocodile',
	weight: '1075 kgs',
	length: '20 ft 3 in'
}

const {name, breed, weight, length} = animal
console.log(`${name} was a ${breed}. He weighed at ${weight} with a measurement of ${length}.`)


const array = [1, 2, 3, 4, 5]
array.forEach((array) => console.log(array));

const reduceNumber = [1, 2, 3, 4, 5]

const reduceArray = reduceNumber.reduce((x, y) => {
	return x + y;
})
console.log(reduceArray);

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}

}

const myDog = new Dog('Frankie', 5, 'Miniature Daschsund');
console.log(myDog);

